export type ControlPoint = {
  id: string;
  controlPointId: string;
  location: [number, number];
  question: string;
  responses: { response: string; right?: boolean | undefined }[];
};
