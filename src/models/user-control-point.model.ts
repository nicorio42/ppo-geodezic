export type UserControlPoint = {
  id: string;
  found: boolean;
  correctAnswer: boolean | null;
};
