import { DOMParser } from 'linkedom';
import { join } from "path";
import { readFileSync, writeFileSync } from "fs"
import { controlCodes } from './controls-codes.js';

const XML_DOC_PATH = join("data", "courses.xml")

const coursesDoc = readFileSync(XML_DOC_PATH).toString();
const parser = new DOMParser();
const document = parser.parseFromString(coursesDoc, 'text/xml');

const controlsTags = document.querySelectorAll("RaceCourseData > Control")

const controls = Array.from(controlsTags).map(tag => {
    const code = tag.querySelector("Id").textContent.trim();
    const positionTag = tag.querySelector("Position");
    const lng = positionTag.getAttribute("lng");
    const lat = positionTag.getAttribute("lat");

    return { code, lng, lat };
})

const coursesTags = document.querySelectorAll("Course")

const courses = Array.from(coursesTags).map(tag => {
    const id = tag.querySelector("Name").textContent.trim();
    const courseControls = Array.from(tag.querySelectorAll("CourseControl"))
        .map(courseControlTag => courseControlTag.querySelector("Control").textContent.trim())

    return { id, controls: courseControls }
})

console.log(controls.length, courses);

const values = Object.values(controlCodes);

controls.forEach((control, index) => {
    const id = values[index]
    writeFileSync(join("src", "content", "control-points", `${control.code}.md`), `---
code: '${control.code.toLocaleLowerCase()}'
id: ${id}
location: ${control.lng},${control.lat}
---
`)
})

/**
 * @param {string[]} controlPoints 
 * @param {string} dirPath 
 */
function generateCourse(controlPoints, dirPath) {
    controlPoints.forEach((controlCode, index) => {
        const control = controls.find(c => c.code === controlCode);

        if (control === undefined) {
            console.warn(`Cannot find control with code ${controlCode} in the control points list.`);
            return;
        }

        writeFileSync(join(dirPath, `${controlCode}.md`), `---
controlPoint: '${controlCode.toLocaleLowerCase()}'
controlNumber: ${index}
question: Lorem, ipsum dolor sit amet consectetur adipisicing elit ?
responses:
  - response: Lorem
  - response: Ipsum
    right: true
  - response: Dolor
  - response: Sit
---

Lorem ipsum, dolor sit amet consectetur adipisicing elit. Perferendis, necessitatibus? Laborum, id! Veniam iure unde dolorum nam nulla, quia facilis molestiae atque ducimus provident accusantium quaerat esse recusandae in. Amet.

Lorem ipsum, dolor sit amet consectetur adipisicing elit. Perferendis, necessitatibus? Laborum, id! Veniam iure unde dolorum nam nulla, quia facilis molestiae atque ducimus provident accusantium quaerat esse recusandae in. Amet.
`)
    })
}

const [history, nature] = courses

if (history !== undefined) {
    generateCourse(history.controls, join("src", "content", "history-adult-course"))
    generateCourse(history.controls, join("src", "content", "history-child-course"))
}

if (nature !== undefined) {
    generateCourse(nature.controls, join("src", "content", "nature-adult-course"))
    generateCourse(nature.controls, join("src", "content", "nature-child-course"))
}

