import { z, defineCollection, reference } from "astro:content";

const controlPoints = defineCollection({
  type: "content",
  schema: z.object({
    code: z.string(),
    id: z.string(),
    location: z.string(),
  }),
});

const natureChildCourse = defineCollection({
  type: "content",
  schema: z.object({
    controlPoint: reference("control-points"),
    controlNumber: z.number(),
    question: z.string(),
    responses: z.array(
      z.object({ response: z.string(), right: z.boolean().optional() })
    ),
  }),
});

const natureAdultCourse = defineCollection({
  type: "content",
  schema: z.object({
    controlPoint: reference("control-points"),
    controlNumber: z.number(),
    question: z.string(),
    responses: z.array(
      z.object({ response: z.string(), right: z.boolean().optional() })
    ),
  }),
});

const historyChildCourse = defineCollection({
  type: "content",
  schema: z.object({
    controlPoint: reference("control-points"),
    controlNumber: z.number(),
    question: z.string(),
    responses: z.array(
      z.object({ response: z.string(), right: z.boolean().optional() })
    ),
  }),
});

const historyAdultCourse = defineCollection({
  type: "content",
  schema: z.object({
    controlPoint: reference("control-points"),
    controlNumber: z.number(),
    question: z.string(),
    responses: z.array(
      z.object({ response: z.string(), right: z.boolean().optional() })
    ),
  }),
});

export const collections = {
  "control-points": controlPoints,
  "nature-child-course": natureChildCourse,
  "nature-adult-course": natureAdultCourse,
  "history-child-course": historyChildCourse,
  "history-adult-course": historyAdultCourse,
};
