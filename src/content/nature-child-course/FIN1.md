---
controlPoint: 'fin1'
controlNumber: 12
question: Lorem, ipsum dolor sit amet consectetur adipisicing elit ?
responses:
  - response: Lorem
  - response: Ipsum
    right: true
  - response: Dolor
  - response: Sit
---

Lorem ipsum, dolor sit amet consectetur adipisicing elit. Perferendis, necessitatibus? Laborum, id! Veniam iure unde dolorum nam nulla, quia facilis molestiae atque ducimus provident accusantium quaerat esse recusandae in. Amet.

Lorem ipsum, dolor sit amet consectetur adipisicing elit. Perferendis, necessitatibus? Laborum, id! Veniam iure unde dolorum nam nulla, quia facilis molestiae atque ducimus provident accusantium quaerat esse recusandae in. Amet.
