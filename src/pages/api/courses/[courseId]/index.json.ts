import { getCollection } from "astro:content";
import type { APIRoute } from "astro";

export const GET: APIRoute = async ({ params: { courseId } }) => {
  if (courseId === undefined) {
    return new Response("Not found", { status: 404 });
  }

  const courseControlPoints = await getCourse(courseId);
  const controlPoints = await getCollection("control-points");

  if (courseControlPoints === undefined) {
    return new Response("Not found", { status: 404 });
  }

  return new Response(
    JSON.stringify(
      courseControlPoints.map((c) => {
        const controlPoint = controlPoints.find(
          (cp) => cp.data.code === c.data.controlPoint.slug
        );

        if (controlPoint === undefined) return {};

        return {
          code: controlPoint.data.code,
          id: controlPoint.data.id,
          location: controlPoint.data.location,
          controlNumber: c.data.controlNumber,
        };
      })
    )
  );
};

function getCourse(courseId: string) {
  if (courseId === "history-adult-course")
    return getCollection("history-adult-course");
  if (courseId === "history-child-course")
    return getCollection("history-child-course");
  if (courseId === "nature-adult-course")
    return getCollection("nature-adult-course");
  if (courseId === "nature-child-course")
    return getCollection("nature-child-course");
}

export async function getStaticPaths() {
  return [
    { params: { courseId: "history-adult-course" } },
    { params: { courseId: "history-child-course" } },
    { params: { courseId: "nature-adult-course" } },
    { params: { courseId: "nature-child-course" } },
  ];
}
