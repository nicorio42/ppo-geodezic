import { getCollection, getEntry } from "astro:content";
import type { APIRoute } from "astro";

export const GET: APIRoute = async ({
  params: { courseId, controlPointId },
}) => {
  if (courseId === undefined) {
    return new Response("Not found", { status: 404 });
  }

  const courseControlPoint = await getEntry(courseId, controlPointId);

  if (courseControlPoint === undefined) {
    return new Response("Not found", { status: 404 });
  }

  const controlPoints = await getCollection("control-points");

  const controlPoint = controlPoints.find(
    (cp) => cp.data.code === courseControlPoint.data.controlPoint.slug
  );

  if (controlPoint === undefined) {
    return new Response("Not found", { status: 404 });
  }

  return new Response(
    JSON.stringify({
      code: controlPoint.data.code,
      id: controlPoint.data.id,
      location: controlPoint.data.location,
      controlNumber: courseControlPoint.data.controlNumber,
      content: courseControlPoint.body,
    })
  );
};

export async function getStaticPaths() {
  let params: { params: { courseId: string; controlPointId: string } }[] = [];

  const historyAdultCourse = await getCollection("history-adult-course");

  if (historyAdultCourse !== undefined) {
    params = [
      ...params,
      ...historyAdultCourse.map((c) => ({
        params: { courseId: "history-adult-course", controlPointId: c.id },
      })),
    ];
  }
  const historyChildCourse = await getCollection("history-child-course");

  if (historyChildCourse !== undefined) {
    params = [
      ...params,
      ...historyChildCourse.map((c) => ({
        params: { courseId: "history-child-course", controlPointId: c.id },
      })),
    ];
  }

  const natureAdultCourse = await getCollection("nature-adult-course");

  if (natureAdultCourse !== undefined) {
    params = [
      ...params,
      ...natureAdultCourse.map((c) => ({
        params: { courseId: "nature-adult-course", controlPointId: c.id },
      })),
    ];
  }

  const natureChildCourse = await getCollection("nature-child-course");

  if (natureChildCourse !== undefined) {
    params = [
      ...params,
      ...natureChildCourse.map((c) => ({
        params: { courseId: "nature-child-course", controlPointId: c.id },
      })),
    ];
  }

  return params;
}
