import { writable } from "svelte/store";
import type { ControlPoint } from "../../models/control-point.model";

export const controlPointsStore = writable<ControlPoint[]>([]);
