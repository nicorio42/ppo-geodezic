import { writable } from "svelte/store";
import type { UserControlPoint } from "../../models/user-control-point.model";

export const userCourseStore = writable<UserControlPoint[]>([]);
