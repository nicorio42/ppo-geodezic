export const mapCoordinates: [
  [number, number],
  [number, number],
  [number, number],
  [number, number],
] = [
  [2.440829235481103, 48.78322210224286],
  [2.452449909852486, 48.783280782892135],
  [2.4525968752285725, 48.7689951067467],
  [2.4409762008571896, 48.768936426097426],
];

export const center: [number, number] = [
  mapCoordinates.map((l) => l[0]).reduce((a, l) => a + l, 0) / 4,
  mapCoordinates.map((l) => l[1]).reduce((a, l) => a + l, 0) / 4,
];